const db = require("./db");
const Livro = require("./model/Livro");
const Edicao = require("./model/Edicao");
const Disciplina = require("./model/Disciplina");
const LivroDisciplina = require("./model/LivroDisciplina");



const express = require("express");
const cors = require('cors');

const app = express()

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/", (req, res) => {
  res.json("API Livros")
});



app.post("/livro", async (req, res) => {

  try {
    const livro = await Livro.create(req.body);
    res.status(200).send(livro);
  } catch(e) {
    res.status(403).send("Já existe.");
  }
});

app.put("/livro", async (req, res) => {

  try {
    const livro = await Livro.findByPk(req.body.id);
    livro.update(req.body);
    res.status(200).send("Cadastrado com sucesso");
  } catch(e) {
    res.status(403).send("Já existe.");
  }
});

app.delete("/livro/:id", async (req, res) => {
  console.log("Entrou no delete");
  try {
    const livro = await Livro.findByPk(req.params.id);
    if(livro) {
      await livro.destroy();
      res.status(200).send("Removido com sucesso");
    }else {
      res.status(400).send("Não encontrado.")
    }
  } catch(e) {
    res.status(400).send(e);
  }
});


app.get("/livro", async (req, res) => {
  let livros = await Livro.findAll();
  res.json(livros);
  res.status(200);
});

app.get("/livro/:id", async (req, res) => {
  const livro = await Livro.findByPk(req.params.id);
  res.json(livro);
  res.status(200);
});


app.listen(3000, ()=> {
  console.log("API executando!");
});

async function sync() {
  await db.sync({force: true});
}


async function conectar() {
  try {
    await db.authenticate();
    console.log("Sucesso!")
  } catch(e) {
    console.log(e, "Falhou!");
  }
}

async function inserirLivro() {
  	try {
      let l1 = {titulo:"Vamos Programar", ano: 2021, autores:"Alguns", editora:"IFPB"};
      await Livro.create(l1);
    } catch(e) {
      console.log(e, "Falhou!");
    }
}

async function listarLivros() {
  try {
    let livros = await Livro.findAll();
    console.log(livros);
  } catch(e) {
    console.log(e);
  }
}

// conectar();

//sync();
//inserirLivro();
//listarLivros();
