const Sequelize = require("sequelize");

const db = require("../db");

const Livro = db.define("livro",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    titulo: {
      type: Sequelize.STRING,
      allowNull: false
    },
    autores: {
      type: Sequelize.STRING,
      allowNull: false
    },
    editora: {
      type: Sequelize.STRING,
      allowNull: false
    }
  }
);



module.exports = Livro;
