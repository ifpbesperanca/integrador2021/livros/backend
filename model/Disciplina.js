const Sequelize = require("sequelize");

const db = require("../db");

const Disciplina = db.define("disciplina",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    curso: {
      type: Sequelize.STRING,
      allowNull: false
    },
    nome: {
      type: Sequelize.STRING,
      allowNull: false
    },
    suap: Sequelize.INTEGER
  }
);

module.exports = Disciplina;
