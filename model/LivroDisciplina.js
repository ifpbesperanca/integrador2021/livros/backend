const Sequelize = require("sequelize");
const db = require("../db");
const Disciplina = require("./Disciplina");
const Livro = require("./Livro");

const LivroDisciplina = db.define("LivroDisciplina",
    {
        LivroId: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            references: {
                model: Livro, 
                key: 'id'
            }
        },
        DisciplinaId : {
            type: Sequelize.INTEGER,
            primaryKey: true,
            references: {
                model: Disciplina, 
                key: 'id'
            }
        },
        basico: Sequelize.BOOLEAN
    }
);

Disciplina.belongsToMany(Livro, {through:LivroDisciplina});
Livro.belongsToMany(Disciplina, {through:LivroDisciplina});
module.exports = LivroDisciplina;