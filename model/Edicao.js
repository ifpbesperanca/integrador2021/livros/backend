const Sequelize = require("sequelize");

const db = require("../db");
const Livro = require("./Livro")

const Edicao = db.define("edicao",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    isbn: {
      type: Sequelize.STRING,
      allowNull: false
    },
    quantidade: {
      type: Sequelize.STRING,
      allowNull: false
    },
    cotacao: Sequelize.DOUBLE,
    ano: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    capa: {
      type: Sequelize.STRING,
      allowNull: false
    },
    numero: Sequelize.INTEGER
  }
);

Livro.hasMany(Edicao);
Edicao.belongsTo(Livro);

module.exports = Edicao;
