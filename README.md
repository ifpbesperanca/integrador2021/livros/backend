# Back End do Sistema de Livros

    - Sistema criado para gerenciamento dos livros do IFPB Campus Esperança

```
console.log("Vamos programar!")
```

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
```

```plantuml
@startuml

Livro "1" *-- "many" Editora : contains

Class03 o-- Class04 : aggregation

Class05 --> "1" Class06

@enduml
```
